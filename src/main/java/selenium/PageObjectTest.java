package selenium;

import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.notNullValue;
import static org.junit.Assert.assertThat;

import org.junit.Test;

import selenium.pages.LoginPage;

public class PageObjectTest {

    private static final String USERNAME = "user";
    private static final String WRONG_PASSWORD = "2";

    @Test
    public void loginFailsWithFalseGredentials() {
        LoginPage loginPage = LoginPage.goTo();

        loginPage.logInWithExpectingFailure(USERNAME, WRONG_PASSWORD);

        assertThat(loginPage.getErrorMessage(), is(notNullValue()));
    }

}
