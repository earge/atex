package custommock;

import static org.junit.Assert.*;

import org.junit.Test;

import common.*;

public class TransferServiceTest {

    @Test
    public void transfersWithCurrencyConversion() {

        TestableBankService bankService = new TestableBankService();
        TransferService transferService = new TransferService(bankService);

        // E_123 konto valuuta on EUR
        // S_456 konto valuuta on SEK

        // kanda 1 EUR kontolt E_123 kontole S_456
        transferService.transfer(new Money(1, "EUR"), "E_123", "S_456");


        assertTrue(bankService.wasCreditCalledWith(new Money(1, "EUR"), "E_123"));
        assertFalse(bankService.wasCreditCalledWith(null, null));

        assertTrue(bankService.wasDebitCalledWith(new Money(10, "SEK"), "S_456"));
        assertFalse(bankService.wasDebitCalledWith(null, null));
    }

    @Test
    public void doesNotTransferWhenNoFounds() {

        TestableBankService bankService = new TestableBankService();
        bankService.setSufficentFundsAvailable(false);

        TransferService transferService = new TransferService(bankService);

        transferService.transfer(new Money(1, "EUR"), "E_123", "S_456");

        assertFalse(bankService.wasCreditCalled());
    }

}